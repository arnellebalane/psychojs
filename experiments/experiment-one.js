var images = ['i-one', 'i-two', 'i-three', 'i-four', 'i-five', 'i-six',
    'i-seven', 'i-eight'];


function shuffle(array) {
    array.sort(function(a, b) {
        return Math.random() - 0.5;
    });
    return array;
}


var experiment = new Psycho({
    interStimulusInterval: 700,
    trialInterval: 1000,
    totalTrials: 60,
    practiceTrials: 12,
    memoryDisplay: function(done) {
        images = shuffle(images);
        $('.memory-display span').each(function(index) {
            $(this).removeClass(images.join(' ')).addClass(images[index]);
        });
        $('.memory-display').removeClass('hidden');
        setTimeout(function() {
            $('.memory-display').addClass('hidden');
            done();
        }, 500);
    },
    testDisplay: function(done) {
        var icon = images[Math.floor(Math.random() * images.length)];
        $('.test-display span').removeClass(images.join(' ')).addClass(icon);
        $('.test-display').removeClass('hidden');
        $('.test-display .action').on('click', function() {
            $('.test-display').addClass('hidden');
            done({
                correct: images.indexOf(icon) < 6 ? 'yes' : 'no',
                answer: $(this).data('answer')
            });
        });
    },
    feedback: function(correct, done) {
        $('.feedback').removeClass('correct wrong')
            .addClass(correct ? 'correct' : 'wrong');
        setTimeout(function() {
            $('.feedback').removeClass('hidden');
            setTimeout(function() {
                $('.feedback').addClass('hidden');
                done();
            }, 1000);
        }, 500);
    },
    beforeActualTrials: function(done) {
        $('.message').empty();
        $('.message').append('<p>The practice trials are over. The actual '
            + 'trials will now begin.</p>');
        $('.message').append('<p>Press SPACE when you are ready.</p>');
        $('.message').removeClass('hidden');
        $(document).on('keypress', function(e) {
            if (e.keyCode === 32) {
                $(document).off('keypress');
                $('.message').addClass('hidden');
                done();
            }
        });
    },
    experimentStart: function(done) {
        $('.message').empty();
        $('.message').append('<p>Press SPACE to start.</p>');
        $('.message').removeClass('hidden');
        $(document).on('keypress', function(e) {
            if (e.keyCode === 32) {
                $(document).off('keypress');
                $('.message').addClass('hidden');
                done();
            }
        });
    },
    experimentDone: function(done) {
        $('.message').empty();
        $('.message').append('<p>Done! Thank you for participating!</p>');
        $('.message').append('<p>Click to download results.</p>');
        $('.message').removeClass('hidden');
        $(document).on('click', done);
    }
});

experiment.run();
