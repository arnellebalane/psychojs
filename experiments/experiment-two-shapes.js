var images = ['i-one', 'i-two', 'i-three', 'i-four', 'i-five', 'i-six',
    'i-seven', 'i-eight'];
var colors = ['blue', 'brown', 'green', 'orange', 'pink', 'purple',
    'red', 'yellow'];
var cues = ['one', 'three', 'five', 'seven', 'nine', 'eleven'];


function shuffle(array) {
    array.sort(function(a, b) {
        return Math.random() - 0.5;
    });
    return array;
}


function getCueChoices(cue) {
    if (cue === 'one') {
        return [0, 5];
    } else if (cue === 'three') {
        return [0, 1];
    } else if (cue === 'five') {
        return [1, 2];
    } else if (cue === 'seven') {
        return [2, 3];
    } else if (cue === 'nine') {
        return [3, 4];
    } else if (cue === 'eleven') {
        return [4, 5];
    }
}


var feature = 'color';

var experiment = new Psycho({
    interStimulusInterval: 700,
    trialInterval: 1000,
    totalTrials: 60,
    practiceTrials: 12,
    memoryDisplay: function(done) {
        images = shuffle(images);
        colors = shuffle(colors);
        feature = 'shape';
        if (feature === 'color') {
            $('.memory-display span').each(function(index) {
                $(this).removeClass(images.join(' ') + ' ' + colors.join(' '))
                    .addClass(images[0]).addClass(colors[index]);
            });
        } else {
            $('.memory-display span').each(function(index) {
                $(this).removeClass(images.join(' ') + ' ' + colors.join(' '))
                    .addClass(images[index]).addClass(colors[0]);
            });
        }
        $('.memory-display').removeClass('hidden');
        setTimeout(function() {
            $('.memory-display').addClass('hidden');
            done();
        }, 500);
    },
    cueDisplay: function(done) {
        cues = shuffle(cues);
        $('.cue-display .cue').removeClass(cues.join(' ')).addClass(cues[0]);
        $('.cue-display').removeClass('hidden');
        setTimeout(function() {
            $('.cue-display').addClass('hidden');
            done();
        }, 100);
    },
    testDisplay: function(done) {
        var icon = '';
        if (feature === 'color') {
            icon = colors[Math.floor(Math.random() * images.length)];
            $('.test-display span').removeClass(images.join(' ') + ' '
                + colors.join(' ')).addClass(images[0]).addClass(icon);
        } else {
            icon = images[Math.floor(Math.random() * colors.length)];
            $('.test-display span').removeClass(images.join(' ') + ' '
                + colors.join(' ')).addClass(colors[0]).addClass(icon);
        }
        $('.test-display').removeClass('hidden');
        $('.test-display .action').on('click', function() {
            $('.test-display').addClass('hidden');
            var choices = getCueChoices(cues[0]);
            done({
                correct: (feature === 'color' && (icon === colors[choices[0]]
                    || icon === colors[choices[1]]))
                    || (feature === 'shape' && (icon === images[choices[0]]
                    || icon === images[choices[1]]))
                    ? 'yes' : 'no',
                answer: $(this).data('answer')
            });
        });
    },
    feedback: function(correct, done) {
        $('.feedback').removeClass('correct wrong')
            .addClass(correct ? 'correct' : 'wrong');
        setTimeout(function() {
            $('.feedback').removeClass('hidden');
            setTimeout(function() {
                $('.feedback').addClass('hidden');
                done();
            }, 1000);
        }, 500);
    },
    beforeActualTrials: function(done) {
        $('.message').empty();
        $('.message').append('<p>The practice trials are over. The actual '
            + 'trials will now begin.</p>');
        $('.message').append('<p>Press SPACE when you are ready.</p>');
        $('.message').removeClass('hidden');
        $(document).on('keypress', function(e) {
            if (e.keyCode === 32) {
                $(document).off('keypress');
                $('.message').addClass('hidden');
                done();
            }
        });
    },
    experimentStart: function(done) {
        $('.message').empty();
        $('.message').append('<p>Kindly listen to the instructions as they '
            + 'dictate the rules for this experiment</p>');
        $('.message').append('<p>Press SPACE to start.</p>');
        $('.message').removeClass('hidden');
        $(document).on('keypress', function(e) {
            if (e.keyCode === 32) {
                $(document).off('keypress');
                $('.message').addClass('hidden');
                done();
            }
        });
    },
    experimentDone: function(done) {
        $('.message').empty();
        $('.message').append('<p>Done! Thank you for participating!</p>');
        $('.message').append('<p>Click to download results.</p>');
        $('.message').removeClass('hidden');
        $(document).on('click', done);
    }
});

experiment.run();
