(function(root, definition) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], definition);
    } else {
        root.Psycho = definition(root.jQuery);
    }
})(this, function($) {
    function EventEmitter() {
        this.listeners = {};
    }

    EventEmitter.prototype.on = function(name, callback) {
        var names = name.split(/ /g);
        for (var i = 0; i < names.length; i++) {
            if (!this.listeners.hasOwnProperty(names[i])) {
                this.listeners[names[i]] = [];
            }
            this.listeners[names[i]].push(callback);
        }
    };

    EventEmitter.prototype.emit = function(name, data) {
        if (this.listeners.hasOwnProperty(name)) {
            for (var i = 0; i < this.listeners[name].length; i++) {
                this.listeners[name][i](data);
            }
        }
    };


    function Psycho(options) {
        if (!(this instanceof Psycho)) {
            return new Psycho(options);
        }

        this.options = this.validateOptions(options);
        this.answers = [];
        this.listeners = {};
    }
    Psycho.prototype = new EventEmitter();

    Psycho.prototype.validateOptions = function(options) {
        var requiredOptions = ['memoryDisplay', 'interStimulusInterval',
            'testDisplay', 'feedback', 'trialInterval', 'totalTrials',
            'practiceTrials', 'beforeActualTrials', 'experimentStart',
            'experimentDone'];
        for (var i = 0, l = requiredOptions.length; i < l; i++) {
            var option = requiredOptions[i];
            if (!options.hasOwnProperty(option)) {
                throw new Error('Option "' + option + '" is not set.');
            }
        }
        return options;
    };

    Psycho.prototype.memoryDisplay = function() {
        return new Promise(function(resolve, reject) {
            this.options.memoryDisplay(resolve);
        }.bind(this));
    };

    Psycho.prototype.interStimulusInterval = function() {
        return new Promise(function(resolve, reject) {
            setTimeout(resolve, this.options.interStimulusInterval);
        }.bind(this));
    };

    Psycho.prototype.cueDisplay = function() {
        return new Promise(function(resolve, reject) {
            this.options.cueDisplay(resolve);
        }.bind(this));
    };

    Psycho.prototype.distractor = function() {
        return new Promise(function(resolve, reject) {
            this.options.distractor(resolve);
        }.bind(this));
    };

    Psycho.prototype.testDisplay = function() {
        return new Promise(function(resolve, reject) {
            this.options.testDisplay(resolve);
        }.bind(this));
    };

    Psycho.prototype.feedback = function(answer) {
        return new Promise(function(resolve, reject) {
            this.answers.push(answer);
            var correct = answer.answer === answer.correct;
            this.options.feedback(correct, resolve);
        }.bind(this));
    };

    Psycho.prototype.beforeActualTrials = function() {
        return new Promise(function(resolve, reject) {
            this.options.beforeActualTrials(resolve);
        }.bind(this));
    };

    Psycho.prototype.experimentStart = function() {
        return new Promise(function(resolve, reject) {
            this.options.experimentStart(resolve);
        }.bind(this));
    };

    Psycho.prototype.experimentDone = function() {
        return new Promise(function(resolve, reject) {
            this.options.experimentDone(resolve);
        }.bind(this));
    };

    Psycho.prototype.generateResults = function() {
        return new Promise(function(resolve, reject) {
            var content = ''
                + '<html>'
                    + '<head>'
                        + '<title>Experiment Results</title>'
                        + '<style>'
                            + 'ul {'
                                + 'list-style: none;'
                                + 'padding: 0;'
                                + 'margin: 0;'
                            + '}'
                            + 'li {'
                                + 'font: normal 18px/30px sans-serif;'
                                + 'color: #fff;'
                            + '}'
                            + 'li::before {'
                                + 'content: attr(data-item);'
                                + 'display: inline-block;'
                                + 'width: 40px;'
                                + 'font: normal 18px/30px sans-serif;'
                                + 'color: #fff;'
                                + 'text-align: right;'
                                + 'padding-right: 10px;'
                                + 'margin-right: 25px;'
                                + 'background-color: rgba(100, 100, 100, 0.5)'
                            + '}'
                            + 'li.trial {'
                                + 'background-color: #333 !important;'
                            + '}'
                            + 'li.correct {'
                                + 'background-color: #4CAF50;'
                            + '}'
                            + 'li.wrong {'
                                + 'background-color: #F44336;'
                            + '}'
                        + '</style>'
                    + '</head>'
                    + '<body>'
                        + '<ul>';
            for (var i = 0; i < this.answers.length; i++) {
                var answer = this.answers[i];
                var correct = answer.correct === answer.answer;
                var li = '<li data-item="' + (i + 1) + '" class="'
                    + (correct ? 'correct' : 'wrong')
                    + (i < this.options.practiceTrials ? ' trial' : '') + '">'
                    + answer.answer + '</li>';
                content += li;
            }
            content += ''
                        + '</ul>'
                    + '</body>'
                + '</html>';

            content = 'data:text/html;charset=utf-8,'
                + encodeURIComponent(content);
            var trigger = document.createElement('a');
            trigger.href = content;
            trigger.download = 'results.html';
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            trigger.dispatchEvent(event);
        }.bind(this));
    };

    Psycho.prototype.trial = function() {
        return new Promise(function(resolve, reject) {
            var chain = this.memoryDisplay()
                .then(this.interStimulusInterval.bind(this));
            if (this.options.hasOwnProperty('cueDisplay')) {
                chain = chain.then(this.cueDisplay.bind(this))
                    .then(this.interStimulusInterval.bind(this));
            }
            if (this.options.hasOwnProperty('distractor')) {
                chain = chain.then(this.distractor.bind(this));
            }
            chain.then(this.testDisplay.bind(this))
                .then(this.feedback.bind(this))
                .then(function() {
                    this.emit('trialend');
                }.bind(this))
                .then(resolve);
        }.bind(this));
    };

    Psycho.prototype.run = function() {
        return new Promise(function(resolve, reject) {
            var trial = 0;
            this.on('start trialend', function() {
                if (trial === this.options.practiceTrials) {
                    this.beforeActualTrials()
                        .then(this.trial.bind(this));
                } else if (trial === this.options.totalTrials) {
                    this.emit('end');
                } else {
                    this.trial();
                }
                trial++;
            }.bind(this));

            this.on('end', function() {
                this.experimentDone()
                    .then(this.generateResults.bind(this))
                    .then(resolve);
            }.bind(this));

            this.experimentStart().then(function() {
                this.emit('start');
            }.bind(this));
        }.bind(this));
    };


    return Psycho;
});
